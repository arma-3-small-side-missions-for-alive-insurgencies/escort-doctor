["HQ", "Aboard mission, I say again ABOARD MISSION."] remoteExec ["BIS_fnc_showSubtitle", -2, false]; // Show subtitle to aboard the mission.

sleep 10;

["escortDoc", "CANCELED", true] call BIS_fnc_taskSetState; // Cancel the parent task.

if (!(("cTask2" call BIS_fnc_taskState) isEqualTo "SUCCEEDED")) then {
  [getPos pp, [side myUnit], hChangePatKill] remoteExecCall ["ALIVE_fnc_updateSectorHostility", 2, false]; // Get a negative impact on Civilian Hostility on patient's sector if the patient hasn't been treated.
};

[getPos dp, [side myUnit], hChangeDocKill] remoteExecCall ["ALIVE_fnc_updateSectorHostility", 2, false]; // Get a negative impact on Civilian Hostility on doctor's sector, no matter what happened with doctor.

_nil = execVM "missions\escortDoc\clearEscortDocMission.sqf"; // Give the scheduler the script to clear and reset the mission.
