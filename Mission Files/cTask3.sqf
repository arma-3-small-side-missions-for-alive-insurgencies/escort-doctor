_cTask3Desc = _this select 0;
_cTask3Title = _this select 1;
// Create the 3rd child task (escort the doctor back to starting point).
[
  (side myUnit), // Task owner.
  ["cTask3", "escortDoc"], // Task ID [taskID, parentTaskID].
  [
    (str (formatText["%1", _cTask3Desc])), // Description.
    _cTask3Title, // Title.
    "" // Markers.
  ],
  (getPos dp), // Destination.
  "ASSIGNED", // State.
  10, // Priority.
  false, // Show notification.
  "run", // Type (icon).
  false // Visible in 3D.
] call BIS_fnc_taskCreate;
