/*================================================================== INSTUCTIONS & INFO ===================================================================================
* For this script to work you need to set down 2 different kind of objects around the map on the 3den editor.
* Objects are recommended since with objects we can use 3 dimensions to place our spawned units,
* something that is NOT possible with markers set on 3den Editor. Author of this script used objects called "hellpers".
* The first set of objects that we have to set are the possible spawning points of the PATIENT and must
* be named pp1-pp_x and the second set is the possible spawning points for the DOCTOR (in this script 2
* possible spawning positions are used for the DOCTOR) and they must be named dp1-dp_x.
* Also, the task descriptions and the task names are customizable from the mission makers with some
* basic knowledge on scripting on the first section of this script (right under the one you are reading now)
* with all of them having a "default value".
*
*
* Things that are customizable on the first section (default values on parenthesis):
*
* Classes/classNames for the DOCTOR and the PATIENT. If more than one is selected, one will be randomly picked
* every time the mission/script is called. (2 for each).
*
* Change the pics in the tasks for identification of both DOCTOR and PATIENT. (default pics depending on the className of doctro and patient)
*
* Doctor will have a helmet and a vest. The classNames of these are customizable in case someone wants to use community mods.
*
* Number of possible spawning positions for the doctor (4).
*
* The time the doctor have to stay close to the patient to "treat" him in seconds. If more than one is given
* one will be picked randomly every time the mission/script is called. (5 different/ from 60 to 300)
*
* Descriptions and titles for the tasks. (a small general description has been set by the author)
*
*/

/*==================================================== Mission parameters set from player (with default values for vanilla game) ===========================================*/
/*==========================================================================================================================================================================*/
/*==========================================================================================================================================================================*/

/*================================================================= DOCTOR's PARAMETERS ====================================================================================*/

// Set the classNames for the doctor.
private _dType = [
  "C_scientist_01_formal_F",
  "C_scientist_02_formal_F",
  "C_Man_Paramedic_01_F"
] call BIS_fnc_selectRandom; // Randomly select doctor's type/className.

// Set the classNames for doctor's helmet, facewear and vest.
private _docHelm = "H_PASGT_basic_white_F"; // Doctor's helmet.
private _docVest = "V_EOD_IDAP_blue_F"; // Doctor's vest.
private _docFaceWear = [
  "G_Respirator_blue_F",
  "G_Respirator_white_F",
  "G_Respirator_yellow_F"
] call BIS_fnc_selectRandom; // Randomly select the color of Doctor's facewear/mask.


/*================================================================= PATIENT's PARAMETERS ===================================================================================*/

// Set the classNames for the patient.
private _pType = [
  "C_IDAP_Man_AidWorker_01_F",
  "C_IDAP_Man_AidWorker_09_F"
] call BIS_fnc_selectRandom; // Randomly select patient's type/className.


/*================================================================= POSITION's PARAMETERS ====================================================================================*/

// The names of the objects/game logics that will be used as possible spawn points for the patient.
pp = [
  pp1,
  pp2,
  pp3,
  pp4,
  pp5
] call BIS_fnc_selectRandom; // Randomly select the position the patient will spawn.
publicVariable "pp";



// Possible locations that doctor may spawn.(Default 1)
private _possibleDocLoc = 1;

/*
In case there are 2 (and customizable for more) positions that the doctor will be able to spawn,
the closest to the patient's position will be chosen. Else, only one will be used.
*/
if (_possibleDocLoc == 2) then {
    if ((dp1 distance pp) < (dp2 distance pp)) then {
      dp = dp1}
    else {
      dp = dp2
    };
  }
else {
  dp = dp1
};
publicVariable "dp";



/*================================================================= TIMER's PARAMETERS ====================================================================================*/

// Randomly select from an array the time (in seconds) the doctor will need to treat his patient.
private _randHealTime = [
60,
120,
180,
240,
300
] call BIS_fnc_selectRandom;


/*================================================================= TASKS' DESCRIPTIONS ====================================================================================*/

// Description and title for the parent task (the whole side mission's task).
private _pTaskTitle = "Medical Emergency"; // Title.
private _pTaskDescr =   // Description.
"As you all know we support IDAP on their efforts to help the local civilians and
lower the impact armed conflicts have on this region. Volunteers are spread
on the villages and the cities of this region and try to provide any kind of
help any kind of aid they can to the local population. This time though,
one of the volunteers is in need of medical care for unknown reason. It can be
excange of fire from enemy faction with friendlies or even some injury on his work
or it can even be a simple sickness. We do not have any intel on this.Our job now is
to go to the designated location, pick up a doctor, escort the doctor to his patient
and when he finish with his treatment, bring him back safely.";


// Description and title for the 1st child task (to pick up the doctor).
private _cTask1Title = "Pick up the Doctor"; // Title.
private _cTask1Desc =  // Description.
"Alright. It is pretty simple, move to the designated position and speak to (interact with)
the doctor to pick him up. Pretty straight forward.";

// Change the image of the doctor in 1st child task description according to className.
switch (_dType) do {
  case "C_scientist_01_formal_F": {
    docImage = "missions\escortDoc\images\doc1.jpg"
  };

  case "C_scientist_02_formal_F": {
    docImage = "missions\escortDoc\images\doc2.jpg"
  };

  case "C_Man_Paramedic_01_F": {
    docImage = "missions\escortDoc\images\doc3.jpg"
  };
};




// Description and title for the 2nd child task (escort the doctor to patient).
private _cTask2Title = "Move to the Patient"; // Title.
private _cTask2Desc =  // Description.
"This is the place that you have to get the doctor to treat his patient.
REMEMBER: keep the doctor 5m and closer to the patient for as much time he may need
to treat his patient (can be up to 5 minutes) and keep him safe the whole time.";

// Change the image of the patient in 2nd child task description according to className.
switch (_pType) do {
  case "C_IDAP_Man_AidWorker_01_F": {
    patImage = "missions\escortDoc\images\patient1.jpg"
  };

  case "C_IDAP_Man_AidWorker_09_F": {
    patImage = "missions\escortDoc\images\patient2.jpg"
  };
};





// Description and title for the 3rd child task (return the doctor safely to starting position).
cTask3Title = "Escort the Doctor back"; // Title.
publicVariable "cTask3Title";
cTask3Desc =  // Description.
"Nice job. Now get the doctor back to where you picked him up and this will be a 'Job Well Done'.";
publicVariable "cTask3Desc";



/*=========================================================================== ALiVE PARAMETERS ================================================================================*/

/*
* Hostility change that will occure on the sector that the doctor AND the patient spawned if the doctor is killed.
* In this instance, hostility will change on patient's spawning sector only if the doctor will die before "treating"
* the patient. If the patient have been treated on doctor's death, the hostility change will apply only on
* doctor's spawning sector.
*/
hChangeDocKill = 10;
publicVariable "hChangeDocKill";

/*
* Hostility change that will occure on the sector that the patient spawned if the patient is killed,
*ONLY BEFORE treatment or the mission will fail before treatment.
*/
hChangePatKill = 20;
publicVariable "hChangePatKill";

// The hostility change that will occure when the patient will be treated.
hChangePatHeal = -10;
publicVariable "hChangePatHeal";

// NOTE :It is possible at least to of the afforementioned changes to take place in a mission's play through (e.g. Patient treated and doctor killed on the way back for any reason!)



/*=========================================================================== MISSION SCRIPT ==================================================================================*/
/*=============================================================================================================================================================================*/
/*=============================================================================================================================================================================*/


// Take variables from the execVM/remoteExec.
myUnitEDM = _this select 0;
publicVariable "myUnitEDM";



// Play a subtitle message to the caller of the mission that there is an active mission if there is one.
if (("escortDoc" call BIS_fnc_taskExists) isEqualTo true) exitWith {
  ["Self", "I already have a Doctor to escort, I do not need another one. When I finish this mission, I may come back and take another one."] remoteExec ["BIS_fnc_showSubtitle", myUnit, false];
};





/* ============================================================== Spawn and initialize the doctor ============================================================= */

private _dGr = createGroup [civilian, true]; // Create a group for doctor to join when created.
doctor = _dGr createUnit [_dType, getPosATL dp, [], 2, "NONE"]; // Create doctor.
doctor setVariable ["ALIVE_profileIgnore", true]; // Do not permit profiling of the unit from ALiVE.
removeHeadgear doctor; // Remove doctor's headwear.
removeGoggles doctor; // Remove doctor's facewear.
removeVest doctor; // Remove possible vest from doctor.
doctor addHeadgear _docHelm; // Add a helmet/headgear to the doctor.
doctor addGoggles _docFaceWear; // Add a mask to the doctor.
doctor addVest _docVest; // Add a vest to the doctor.
doctor disableAI "MOVE"; // Disables doctor's ability to move.
doctor disableAI "AUTOCOMBAT"; // Disables doctor's ability to "AUTOCOMBAT".
doctor setDir (random 360); // Set a random facing for the doctor.
publicVariable "doctor"; // Broadcast the variable so it will become public and updated.




// Add an EH to the doctor so the mission will fail in case he dies.
[doctor, ["Killed", {
      [doctor, "animDone"] remoteExec ["removeAllEventHandlers", 0, true]; // Remove EH that loops the animation.
      [doctor,""] remoteExec ["switchMove", 0, true]; // Disables the animation that was playing.
      ["escortDoc", "FAILED", true] call BIS_fnc_taskSetState; // Set the parent task (so all the children tasks too) as "FAILED".
      _nil = [] execVM "clearEscortDocMission.sqf"; // Execute the script that clears and resets the mission.
      [getPos dp, [side myUnitEDM], hChangeDocKill] remoteExecCall ["ALIVE_fnc_updateSectorHostility", 2, false]; // ALiVE Civilians hostility change.
      if (!(("cTask2" call BIS_fnc_taskState) isEqualTo ("SUCCEEDED"))) then {[getPos pp, [side myUnitEDM], hChangePatKill] remoteExecCall ["ALIVE_fnc_updateSectorHostility", 2, false];};
    }
  ]
] remoteExec ["addEventHandler", 0, true]; // Remotely execute this EH to every machine.





// Add a hold action to the doctor for players to "recruit" him.
[
doctor, // Target.
"Doctor join your group", // Title.
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_requestleadership_ca.paa", // Idle icon.
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_thumbsup_ca.paa", // Progress icon.
"_this distance doctor <= 2.5", // Condition show.
"_caller distance doctor <= 2.5", // Condition progress.
{["Doctor", "Alright I am ready to go."] remoteExec ["BIS_fnc_showSubtitle", _caller, false];}, // Code start.
{}, // Code progress.
{
  doctor enableAI "MOVE"; // Enable movement on the doctor again.
  [doctor, "animDone"] remoteExecCall ["removeAllEventHandlers", 0, true]; // Remove the EH that loops the animation on the doctor.
  [doctor] joinSilent (group _caller); // Doctor joins player's group.
  [doctor, ""] remoteExecCall ["switchMove" , 0, true]; // Instantly removes the animation of the doctor to all client machines (and the server).
  ["cTask1", "SUCCEEDED", true] call BIS_fnc_taskSetState; // Set the 1st task (to pick up the doc) to "SUCCEEDED".
  ["Doctor", "Alright, I am ready. Let's go."] remoteExec ["BIS_fnc_showSubtitle", _caller, false]; // Show subtitles.
  [_target, _actionID] remoteExec ["BIS_fnc_holdActionRemove", -2, true]; // Remove the holdAction from everyone after the doc has joined player's group.
}, // Code completed.
{}, // Code interrupted.
[], // Arguments.
3, // Duration.
100, // Priority.
false, // Remove completed.
false, // Show unconsious.
true // Show window.
] remoteExec ["BIS_fnc_holdActionAdd", -2, true]; // Adding the hold action to every client.






//Set a random animation loop to the doctor.
private _randDocAnim = [
  "Acts_AidlPercMstpSnonWnonDnon_warmup_3_loop",
  "Acts_A_M01_briefing",
  "Acts_AidlPercMstpSnonWnonDnon_warmup_8_loop",
  "Acts_Briefing_Intro3_Physicist_1",
  "Acts_CivilListening_1",
  "Acts_CivilListening_2",
  "Acts_CivilTalking_1",
  "Acts_CivilTalking_2"
] call BIS_fnc_selectRandom; // Randomly select an animation from the array.

[doctor, _randDocAnim] remoteExecCall ["switchMove", 0, true]; // play the randomly selected animation on the doctor.

// Add the EH that will loop the animation on the doctor.
[doctor, ["animDone", {
  params ["_unit", "_anim"]; // Setting the parameters for the EH.
  private _randDocAnim = [
        "Acts_AidlPercMstpSnonWnonDnon_warmup_3_loop",
        "Acts_A_M01_briefing",
        "Acts_AidlPercMstpSnonWnonDnon_warmup_8_loop",
        "Acts_Briefing_Intro3_Physicist_1",
        "Acts_CivilListening_1",
        "Acts_CivilListening_2",
        "Acts_CivilTalking_1",
        "Acts_CivilTalking_2"
      ] call BIS_fnc_selectRandom; // Randomly select the next animation that will play if the previous one was not a loop.
      _unit switchMove _randDocAnim; // Change the animation to the newly randomly selected one.
    }
  ]
] remoteExecCall ["addEventHandler", 0, true]; // Add the EH to every machine.





/* ============================================================== Spawn and initialize the patient ============================================================= */

private _pGr = createGroup [civilian, true]; // Create a group for the patient to join when created.
patient = _pGr createUnit [_pType, getPosATL pp, [], 0, "NONE"]; // Create patient.
patient setVariable ["ALIVE_profileIgnore", true]; // Do not permit profiling of the unit from ALiVE.
removeHeadgear patient; // Remove patient's headwear.
removeVest patient; // Remove possible vest from patient.
removeBackpack patient; // Remove possible backpack from patient.
removeGoggles patient; // Remove possible facewear from patient.
patient disableAI "MOVE"; // Disables patient's ability to move.
patient disableAI "AUTOCOMBAT"; // Disables patient's ability to "AUTOCOMBAT".
patient setDir (getDir pp); // Set patient's facing the same as object's that is used to get spawn position.
publicVariable "patient"; // Broadcast patient's variably so it can be accessed by every machine in case it changes locality.





// Add a random animation to the patient.
private _randPatAnim = [
  "Acts_CivilInjuredArms_1",
  "Acts_CivilInjuredChest_1",
  "Acts_CivilInjuredGeneral_1",
  "Acts_CivilInjuredHead_1",
  "Acts_CivilInjuredLegs_1"
] call BIS_fnc_selectRandom; // Randomly select an animation from the array.

[patient, _randPatAnim] remoteExecCall ["switchMove", 0, true]; // Instantly execute the animation on every machine.

// Add an EH on the patient to loop the random animation until he is "treated" by the doctor.
[patient, ["animDone", {
      params ["_unit", "_anim"]; // Setting the params for the EH
      _unit switchMove _anim; // Actually repeating the same animation that was played from the beggining.
    }
  ]
] remoteExecCall ["addEventHandler", 0, true]; // Add the EH on every machine.




// Add an EH for the mission to fail if the patient is killed.
[patient, ["Killed", {
      [patient, "animDone"] remoteExecCall ["removeAllEventHandlers", 0, true]; // Remove the EH that made the animation loop otherwise, the death animation will be played after the random animimation finish.
      [patient,""] remoteExecCall ["switchMove", 0, true]; // Instantly diables the animation on all machines.
      ["escortDoc", "FAILED", true] call BIS_fnc_taskSetState; // Set the parent task's (so children's task too) state as "FAILED".
      _nil = [] execVM "clearEscortDocMission.sqf"; // Call the script that clear and resets the mission.
      [getPos pp, [side myUnitEDM], hChangePatKill] remoteExecCall ["ALIVE_fnc_updateSectorHostility", 2, false]; // ALiVE Civilians hostility change.
    }
  ]
] remoteExecCall ["addEventHandler", 0, true]; // Add the EH on all machines.








/*=============================================================================== CREATE THE TASKS ========================================================================*/

// Create the parrent task (the actuall mission task).
[
  (side myUnitEDM), // Task owner.
  "escortDoc", // Task ID.
  [
    (str (formatText ["%1", _pTaskDescr])), // Description.
    _pTaskTitle, // Title.
    "" // Markers.
  ],
  objNull, // Destination.
  "CREATED", // State.
  1, // Priority.
  false, // Show notification.
  "danger", // Type (icon).
  false // Visible in 3D.
] call BIS_fnc_taskCreate;




// Create 1st child task (pick up the doc).
[
  (side myUnitEDM), // Task owner.
  ["cTask1", "escortDoc"], // Task ID [taskID, parentTaskID].
  [
    (str (formatText ["%1<br />Doctor: <br /><img image =%2 />", _cTask1Desc, (str docImage)])), // Description.
    _cTask1Title, // Title.
    "" // Markers.
  ],
  (getPos dp), // Destination.
  "ASSIGNED", // State.
  10, // Priority.
  true, // Show notification.
  "talk", // Type (icon).
  false // Visible in 3D.
] call BIS_fnc_taskCreate;




// Create the 2nd child task (escort the doctor to the patient).
[
  (side myUnitEDM), // Task owner.
  ["cTask2", "escortDoc"], // Task ID [taskID, parentTaskID].
  [
    (str (formatText["%1, <br />Patient: <br /> <img image=%2 />", _cTask2Desc, (str patImage)])), // Description.
    _cTask2Title, // Title.
    "" // Markers.
  ],
  (getPos pp), // Destination.
  "CREATED", // State.
  8, // Priority.
  false, // Show notification.
  "heal", // Type (icon).
  false // Visible in 3D.
] call BIS_fnc_taskCreate;







/* ===================================================== Create a trigger to complete the 2nd task and initiate the 3rd ========================================================================*/

/*
* Create the trigger that will check if the doctor is closer < 5m to the patient for a specific amount of time
* to make the 2nd task to "SUCCEED", initate the 3rd task (return Doc) and change to ALiVE Civilian's Hostility
* towards player's side by -10 (scale from -100 (friendly) to 100 (extreme hostility). For more info check
* ALiVE's forums, wiki and Discord Server).
*/
private _healTrig = createTrigger [
  "emptyDetector", // Type (most of the time "emptyDetector").
  [0, 0, 0], // Position.
  false // Make global.
];


// Set the statement of the trigger
_healTrig setTriggerStatements [
  "doctor distance patient <= 5",
  "['cTask2', 'SUCCEEDED', true] call BIS_fnc_taskSetState;
   [patient, 'animDone'] remoteExecCall ['removeAllEventHandlers', 0, true];
   [patient, 'killed'] remoteExecCall ['removeAllEventHandlers', 0, true];
   [[cTask3Desc, cTask3Title], 'cTask3.sqf'] remoteExec ['execVM', 2, false];
   [patient, ''] remoteExec ['playMove', 0, true];
   patient enableAI 'ALL';
   [getPos pp, [side myUnitEDM], hChangePatHeal] remoteExecCall ['ALIVE_fnc_updateSectorHostility', 2, false];",
  ""
];

// Set trigger's time.
_healTrig setTriggerTimeout [_randHealTime, _randHealTime, _randHealTime, true]; // Set the trigger to activate after the random time that was set by the player.







/* =========================================================== Create a trigger to complete the 3rd and the parent task, complete, clear and reset the mission =============================== */

// Create the trigger.
private _endTrig = createTrigger [
  "emptyDetector",
  [0, 0, 0],
  false
];

// Set trigger's timers.
_endTrig setTriggerTimeout [5, 5, 5, false];

// Set trigger's statement.
_endTrig setTriggerStatements [
  "(doctor distance dp <= 15) && (('cTask2' call BIS_fnc_taskState) isEqualTo 'SUCCEEDED')",
  "['cTask3', 'SUCCEEDED', true] call BIS_fnc_taskSetState;
   ['escortDoc', 'SUCCEEDED', true] call BIS_fnc_taskSetState;
   _nil = [] execVM 'clearEscortDocMission.sqf'",
   ""
];
patImage = nil;
docImage = nil;
